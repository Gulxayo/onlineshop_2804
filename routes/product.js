const router = require('express').Router()
const md5 = require('md5')
const multer = require('multer');
const {addProduct,
    getById,
    getProduct,
    updateProduct,
    deleteFilePoster,
    getAllproducts} = require('../controllers/productController');
const path = require('path')

//const {eA,eAdmin,eOperator,eBoth} = require('../middleware/checkUser');


const storage = multer.diskStorage({
    destination: function (req,file,cb) {
        cb(null, './public/uploads/products/org');
    },
    filename: function (req,file,cb) {
        //cb(null, `${Date.now()}-${file.originalname}`);
        cb(null, `${md5(Date.now())}${path.extname(file.originalname)}`);
    }
});


const upload = multer({storage: storage});

//
// router.get('/product/', (req, res) => {
//     res.render('../views/pages/product/product', { title: 'product page', layout:'../views/layout',product:product})
// })


// router.get('/product', (req, res) => {
//     res.render('./pages/detail', {
//         title: 'product page',
//         layout:'./layout' ,
//        // product: product
//     })
// })
// router.get('/product/:id', (req, res) => {
//     res.render('./pages/product', {
//         title: 'Product only',
//         layout:'./layout',
//         product:product
//     })
// })

router.post('/admin/products', upload.array('images',12), addProduct);
router.get('/admin/products', getProduct);
router.get('/admin/products/:id', getById);
router.delete('/admin/products/:id',deleteFilePoster);
router.patch('/admin/products/:id',updateProduct)
module.exports = router;
