const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const pathdir = require('path').join(__dirname, '/public/uploads')
const mongoose = require('mongoose');
const config = require ('./config/server');
const session = require('express-session');
const passport = require('passport')
const cookieParser = require('cookie-parser')
const csrf = require('csurf')
const csrfProtection = csrf({ cookie: true });

//config();
const PORT = 3000;
const app = express();
const expressLayoutes=require('express-ejs-layouts')
//const {check, validationResult}=require('express-validator')
const urlencodedParser=bodyParser.urlencoded({extended:false})
const methodOverride = require("method-override");
app.use(methodOverride("_method", {
    methods: ["POST", "GET"]
}));

mongoose.connect(config.mongoUri,{
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(()=>{
        console.log('Bazaga Ulandi');
    })
    .catch((err)=>{
        console.log('Xatolik', err);
    });

mongoose.set('useFindAndModify', false);
app.use(cookieParser())
//user global
app.get('*',(req,res,next)=>{
    res.locals.user = req.user || null;
    next();
})
app.use(expressLayoutes)
app.use(session({secret:'mysupersecret', resave:false, saveUninitialized:false}))
app.use(bodyParser.json());
app.use(cors({ rogin : "*" }));
app.use('/public/uploads', express.static(pathdir));``
app.all('*', function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

//require('./controllers/authController' )(passport)
app.use(express.json());
app.use(express.urlencoded({extended : true}));
// app.use('/public/uploads', express.static(path));
app.use(express.json());
// Express Session
app.use(session ({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
}))
// Passport middleware
app.use(passport.initialize());
app.use(passport.session())

// EJS Static
app.use(express.static('public/uploads'));
app.use('/uploads', express.static(__dirname + '/public/uploads'))
app.use('/css', express.static(__dirname + '/public/css'))
app.use('/fonts', express.static(__dirname + '/public/fonts'))
app.use('/ico', express.static(__dirname + '/public/ico'))
app.use('/img', express.static(__dirname + '/public/img'))
app.use('/js', express.static(__dirname + '/public/js'))
app.use('/pages', express.static(__dirname + '/public/pages'))
app.use('/scssphp', express.static(__dirname + '/public/scssphp'))
app.use('/ajax', express.static(__dirname + '/public/ajax'))
app.use('/header', express.static(__dirname + '/public/header'))
app.use('/sass', express.static(__dirname + '/public/sass'))
app.use('/assets/plugins', express.static(__dirname + '/public/assets/plugins'))
app.use('/assets/css', express.static(__dirname + '/public/assets/css'))
app.use('/assets/fonts', express.static(__dirname + '/public/assets/fonts'))
app.use('/assets/icon', express.static(__dirname + '/public/assets/icon'))
app.use('/assets/images', express.static(__dirname + '/public/assets/images'))
app.use('/assets/js', express.static(__dirname + '/public/assets/js'))
app.use('/assets/pages', express.static(__dirname + '/public/assets/pages'))
app.use('/assets/scss', express.static(__dirname + '/public/assets/scss'))
// Set Views
app.set('views', './views')
app.set('view engine', 'ejs')

// Navigation

const commentRouter = require('./routes/comment')
const authRouter = require('./routes/auth')
const categoryRouter = require('./routes/category')
const productRouter = require('./routes/product')
const orderRouter = require('./routes/order')
const searchRouter = require('./routes/search');
const statisticsRouter = require('./routes/statistic');
const regionRouter = require('./routes/region');
const userRouter = require('./routes/user')

app.use('/', authRouter);
app.use('/', categoryRouter);
app.use('/', productRouter);
app.use('/',orderRouter);
app.use('/',searchRouter);
app.use('/',statisticsRouter);
app.use('/',commentRouter);
app.use('/',regionRouter);
app.use('/',userRouter)


app.get('/', (req, res) => {
    res.render('./index/index', { title: 'Home ', layout : './layout' } )
})



app.get('/admin', (req, res) => {
    res.render('./admin/adlogin', { title: 'Login page ', layout:'./admin/adlogin' } )
})

app.get('/admin/home', (req, res) => {
    res.render('./admin/dashboard', {
        title:"login page",
        layout:'./admin/admin'
    })
})


app.listen(PORT,()=> {
    console.log('Server running ' + PORT);
})

