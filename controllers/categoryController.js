const Category=require('../models/category')
const Color=require('../models/color')
const Product=require('../models/product')

exports.addCategory=(req,res)=>{
    if(!req.body.name){
        res.json("name required");
    }
    const category=new Category({
        name:req.body.name
    })
    category.save()
        .then(()=>{
            // res.status(200).json({
            //     success:true,
            //     message: "Ушпешно добавления",
            //     category : category
            // })
            // res.render('../views/admin/categorys',
            //     { title: 'categorys page',
            //         layout:'../views/admin/categorys',
            //         category:category
            //     })
            res.redirect('/admin/category')
        })
        .catch((e)=>{
            res.status(500).json({
                success:false,
                e
            })
        })
}

exports.getALLCategory=async (req,res)=>{
    const category=await Category.find()
        .sort({date:-1})
    res.render('./admin/categorys',
        { title: 'categorys page',
            layout:'./admin/admin',
            category:category
        })
    //res.send(category)
}

exports.getById=async (req,res)=>{
    const category = await Category.findById(req.params.id)
    res.render('./admin/editCategory', {
        title: 'Product only',
        layout:'./admin/admin',
        category: category
    })
    //  res.status(200).json({
    //      success:true,
    //      category:category
    //  })
     //console.log(category)
}
exports.updateCategory = async(req, res) => {
    const category = await Category.findByIdAndUpdate(req.params.id)
    category.name = req.body.name
    category.save()
        .then(()=>{
            // res.render('../views/admin/categorys',
            //     { title: 'categorys page',
            //         layout:'../views/admin/categorys',
            //         category:category
            //     })
            res.redirect('/admin/category')
        })
        .catch((err)=>{
            res.status(500).json({
                success : false,
                err
            })
        })
}

exports.deleteCategory=async (req,res)=>{
    await Category.findByIdAndDelete(req.params.id, (err,doc)=>{
        if(!err){
          //  res.json({message: "Этот категория был удален"});
          //   res.render('../views/admin/categorys',
          //       { title: 'categorys page',
          //           layout:'../views/admin/categorys',
          //           doc:[]
          //
          //       })
            res.redirect('/admin/category')
        }else{
            console.log("error"+err)
        }
    })
}

exports.addColor= (req,res)=>{
    const color=new Color({
            name: req.body.name,
            url : req.body.url
    })
    color.save()
        .then(()=>{
            res.status(200).json({
                message: "Успешно добавлено",
                color:color
            })
        })
}

exports.getColors = async (req,res) => {
    let colors = await Color.find().sort({date: -1});
    res.send(colors);
}

exports.deleteColor=async (req,res)=>{
    await Color.findByIdAndDelete(req.params.id, (err,doc)=>{
        if(!err){
            res.json({message: "Этот цвет был удален"});
        }else {
            res.json({ error : err })
            console.log("Error" + err);
        }
    })
}



