const Product = require('../models/product')
const fs = require('fs');
const path = require('path')
const sharp=require('sharp')
const multer = require('multer')
exports.addProduct=async (req,res)=>{
    const files = req.files;
    let urls = [];
    let thumb=[]
   // console.log(files)
    let orginal=`/public/uploads/products/org/${files[0].filename}`
    for(let i = 1; i < files.length; i++){
        const {filename} = files[i];

        // thumb.push(`/public/uploads/products/thumb/${filename}`)

        await sharp(path.join(path.dirname(__dirname) + `/public/uploads/products/org/${filename}`) ).resize(500,500)
            .jpeg({
                quality: 60
            })
            .toFile(path.join(path.dirname(__dirname) + `/public/uploads/products/thumb/${filename}`), (err)=>{
                if(err) {
                    throw err
                }
                fs.unlink(path.join(path.dirname(__dirname) + `/public/uploads/products/org/${filename}`)  ,(error)=>{
                    if (error) res.send(error)
                })
            })
        urls.push({
            url:`/public/uploads/products/thumb/${filename}`,
            colorId:req.body.colorId
        })
    }

    const product = new Product({
        name: req.body.name,
        poster:orginal,
        category: req.body.category,
        size: req.body.size,
        diametr: req.body.diametr,
        descriptionUz: req.body.descriptionUz,
        xarakterUz:req.body.xarakterUz,
        images: urls,
        price: req.body.price,
        pid: Date.now(),
        date: Date.now()
    });
   await product.save()
        .then(() => {
            res.status(200).json({
                success: true,
                product: product
            })
        })
       .catch((error)=> {
           res.send(error)
       })
}

exports.getProduct=async (req,res)=>{
    const product= await Product.find()
        //.populate('images.colorId')
        .sort({date:-1})
    res.render('./pages/detail', {
        title: 'product page',
        layout:'./layout' ,
        product: product
    })
    //res.send(product)
    // console.log(product)
}
exports.getById=async (req,res)=>{
    const product = await Product.findById(req.params.id)
        .limit(4);
    res.render('./pages/product', {
        title: 'Product only',
        layout:'layout',
        product: product
    })
   //  res.status(200).json({
   //      success:true,
   //      product:product
   //  })
  // console.log(product)
}
exports.updateProduct = async(req, res) => {

    const product = await Product.findByIdAndUpdate(req.params.id)
    product.price = req.body.price
    product.name=req.body.name
    product.poster=req.body.poster
    product.category= req.body.category
    product.size= req.body.size
    product.diametr= req.body.diametr
    product.descriptionUz=req.body.descriptionUz
    product.xarakterUz= req.body.xarakterUz
    product.save({validateBeforeSave:false})
        .then(()=>{
            res.status(200).json({
                success : true,
                data : product
            })
        })
        .catch((err)=>{
            res.status(500).json({
                success : false,
                err
            })
        })
}
const clearImage = (filePath) => {
    filePath = path.join(__dirname, "..", filePath);
    fs.unlink(filePath, (err) => {
        console.log(err);
    });
};


exports.deleteFilePoster = async (req, res) => {
    await Product.findByIdAndDelete({_id: req.params.id})
        .exec((error,data) => {
            if(error) {
                res.send(error)
            }
            else{
                const isMatch = data.images
                         console.log(isMatch[0].url)
                const thumb = data.poster
                let fileOriginalFirstElelement = path.join(path.dirname(__dirname) + `${thumb}`)

                for(let i = 0; i < isMatch.length; i++){

                    let fileOriginal = path.join(path.dirname(__dirname) + `${isMatch[i].url}`)
                    fs.unlink(fileOriginal, async (error) => {
                        if (error) {
                            console.log(error)
                        }
                        await Product.findByIdAndDelete({_id: req.params.id})
                        fs.unlink (fileOriginalFirstElelement, (error) => {
                            if (error) throw error
                        })
                    })
                }

            }
        })
}