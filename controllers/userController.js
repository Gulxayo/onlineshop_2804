const User = require('../models/user');
const config = require ('../config/server');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

exports.register = async  ( req, res )=>{
    let userData = req.body;
    const salt = await bcrypt.genSalt();
    const password = await bcrypt.hash(userData.password, salt);
    let user = new User({
        name: userData.name,
        email: userData.email,
        password: password,
        phone:userData.phone,
        regionId:req.body.regionId,
        date: Date.now()
    });

    user.save((error, registedUser)=>{
        if(error){
            throw error
        }else{
            let payload = {subject: registedUser._id};
            let token = jwt.sign(payload, config.secret);
            //res.status(200).send({token});
            // res.render('../views/pages/register',{
            //     title:"User Register page",
            //     layout:'../views/layout',
            //     user:user
            // })
            res.redirect('/loginn')
        }
    })
};

exports.loginn = async (req, res) => {
    await User.findOne({ email: req.body.email }, (error, user) => {
        if (error) {
            res.send(error)
        } else {
            if (!user) {
               res.redirect('/loginn')
            } else {
                if (!bcrypt.compareSync(req.body.password, user.password)) {
                    res.redirect('/loginn')

                } else {
                    let payload = { subject: user._id }
                    //let token = jwt.sign(payload, config.JWT_SECRET);


                    res.redirect('/')
                }

            }
        }
    })
}

exports.getAll=async (req,res)=>{
    const users=await User.find()
    .sort({date:-1})
    res.render('./admin/users',
        { title: 'users page',
            layout:'./admin/admin',
            users:users
        })
     //res.status(200).json(users)
   // console.log(users)
}